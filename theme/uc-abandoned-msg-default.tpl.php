Dear [uc_order:dynamic-name],<br /><br />

If you have experienced any difficulty during your checkout process please call us at <strong>[store:phone]</strong> and we will be able to quickly process your order over the phone.<br />
<br /><strong>Your pending order number is [uc_order:order-number] </strong>
<br />
[uc_order:cart]
<br />
Sincerely, <br />
<br />
[site:name] Customer Service<br />
<br />
<p>[site:url]</p>
<p style="text-align:center;">[site:logo]  </p>