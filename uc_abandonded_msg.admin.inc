<?php

function uc_abandonded_msg_admin_form(){
 $form = array();  
 $form['uc_abandonded_msg_history_timeout'] = array(
    '#type' => 'textfield',
    '#title' => t('How many days to go back for Abandoned carts.'),
    '#description' => t('This is to prevent the first run of this module from sending too many messages for abandoned carts from too long ago'),
    '#default_value' => variable_get('uc_abandonded_msg_history_timeout', '7'),
    '#required' => TRUE,
    '#element_validate' => array('element_validate_number'),
  );
  
  $form['uc_abandonded_msg_email'] = array(
    '#type' => 'textfield',
    '#title' => t('From Email'),
    '#required' => TRUE,
    '#default_value' => variable_get('uc_abandonded_msg_email', variable_get('site_mail')),
    '#description' => t('Enter the email address to send the emails from.'),
    '#size' => 40,
    '#maxlength' => 40,
    '#element_validate' => array('uc_abandoned_msg_email_element_validate'),
  );
    
  
  $form['uc_abandonded_msg_email_copy'] = array(
    '#type' => 'checkbox',
    '#title' => t('Send a blind carbon copy of each message to this email address?'),
    '#description' => t('This may be useful to keep an eye on the messages that are sent.'),
    '#default_value'=> variable_get('uc_abandonded_msg_email_copy'),
  );
 

 $form['uc_abandonded_msg_subject'] = array(
    '#type' => 'textfield',
    '#title' => t('Email Subject Line'),
    '#required' => TRUE,
    '#default_value' => variable_get('uc_abandonded_msg_subject', uc_abandoned_msg_load_default('subject')),    
    '#size' => 60,
    '#maxlength' => 80,    
  );
  
  $form['uc_abandonded_msg_limit'] = array(
       '#type' => 'select',
       '#title' => t('Maximum emails to send per cron run.'),
       '#options' => array(
         10 => t('10'),
         50 => t('50'),
         100 => t('100'),
         200 => t('200'),
         300 => t('300'),
         400 => t('400'),
         500 => t('500'),
       ),
       '#default_value' => variable_get('uc_abandonded_msg_limit', '50'),       
   );
  
 $form['uc_abandonded_msg_msg'] = array(
    '#type' => 'textarea',
    '#title' => t('Message Template'),
    '#required' => TRUE,    
    '#default_value' => variable_get('uc_abandonded_msg_msg', uc_abandoned_msg_load_default('body')),
    '#description' => t('The following special tokens may be used: <strong>[uc_order:cart]</strong> (includes a copy of the customer\'s cart) | <strong>[uc_order:dynamic-name]</strong> (inserts the customer\'s first name or, if not present, uses the Fallback name below.)')
  ); 

 $form['uc_abandonded_username_fallback'] = array(
    '#type' => 'textfield',
    '#title' => t('Fallback Customer Name Value'),
    '#description' => t('If the customer\'s first name is not present for the user then the value entered here will be used instead for the [uc_order:dynamic-name] token.'),
    '#required' => FALSE,
    '#default_value' => variable_get('uc_abandonded_username_fallback','Valued Customer'),    
    '#size' => 60,
    '#maxlength' => 80,    
  );  
  
  // Get a list of all formats.
  $formats = filter_formats();
  foreach ($formats as $format) {
    $format_options[$format->format] = $format->name;
  }
  $form['uc_abandonded_msg_format'] = array(
    '#type' => 'select',
    '#title' => t('E-mail format'),
    '#options' => $format_options,
    '#default_value' => variable_get('uc_abandonded_msg_format', filter_fallback_format()),
    '#access' => count($formats) > 1,
    '#attributes' => array('class' => array('filter-list')),
  ); 
  
  $form['uc_abandonded_msg_testmode'] = array(
    '#type' => 'fieldset',
    '#title' => t('Test Mode'),
  );
  $form['uc_abandonded_msg_testmode']['uc_am_testmode_active'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable Test Mode'),
    '#default_value' => variable_get('uc_am_testmode_active', 1),
    '#description' => t('When test mode is active all abandoned carts messages will be sent to the test email address instead of cart owner for testing purposes. When in test module the status of the message is not updated so the same messages will be sent on each cron run.'),
  );
   $form['uc_abandonded_msg_testmode']['uc_am_testmode_email'] = array(
    '#type' => 'textfield',
    '#title' => t('Test Mode Email'),
    '#required' => TRUE,
    '#default_value' => variable_get('uc_am_testmode_email', variable_get('site_mail')),
    '#description' => "Enter the email address to send the test emails to.",
    '#size' => 40,
    '#maxlength' => 40,
    '#element_validate' => array('uc_abandoned_msg_email_element_validate'),
    '#states' => array(
      'visible' => array(
        ':input[name="uc_am_testmode_active"]' => array('checked' => TRUE),
        ),
      ),
  );  
  
  return system_settings_form($form);      
}


function uc_abandoned_msg_email_element_validate($element, &$form_state, $form) {
   if (!valid_email_address($element['#value'])) {
     form_error($element, t('Please enter a valid email address.'));
   }
}